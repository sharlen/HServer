package top.hserver.core.interfaces;

import top.hserver.core.server.context.Webkit;

public abstract class Limit {

    protected abstract void result(Webkit webkit, boolean status);

}
